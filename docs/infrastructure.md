# elb-log-shipper / infrastructure


## AWS


### S3 Bucket(s)

Create an S3 Bucket or Buckets for storing access logs

You will need to grant AWS permission to write access logs, see:

-   [ELB: Attach a Policy to Your S3 Bucket](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-access-logs.html#attach-bucket-policy)

-   [ALB: Bucket Permissions](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-access-logs.html#access-logging-bucket-permissions)

These instructions should be the same regardless of the load balancer type,
and you may use a single S3 Bucket for access logs from a mix of different load balancers types


### Elastic / Application Load Balancer(s)

See:

-   [Access Logs for Your Classic Load Balancer](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/access-log-collection.html)

-   [Access Logs for Your Application Load Balancer](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/load-balancer-access-logs.html)

Your load balancers should have Access Logs enabled,
and should store them in the designated S3 Bucket or multiple Buckets


### SNS Topic

This is theoretically optional,
although our [main.go](../cmd/elb-log-shipper/main.go) implementation requires it.

S3 can send event notifications directly to either SNS or SQS,
so your own implementation may well skip the SNS indirection.

If you do decide to use SNS the way we do,
then ensure that your S3 Bucket(s) has permission to send notifications to your SNS Topic(s):

-   [Example Walkthrough: Create an Amazon SNS Topic](https://docs.aws.amazon.com/AmazonS3/latest/dev/ways-to-add-notification-config-to-bucket.html#step1-create-sns-topic-for-notification)


### SQS Queue

Make sure the elb-log-shipper has "sqs:DeleteMessage*" and "sqs:ReceiveMessage" permissions.

If you decide to simplify the data flow by omitting SNS,
then ensure that your S3 Bucket(s) has permission to send notifications to your SQS Queue:

-   [Example Walkthrough: Create an Amazon SQS Queue](https://docs.aws.amazon.com/AmazonS3/latest/dev/ways-to-add-notification-config-to-bucket.html#step1-create-sqs-queue-for-notification)


### S3 Bucket Event Notifications

Configure your S3 Bucket(s) to notify either SNS or SQS as appropriate to your implementation:

-   [Example Walkthrough: Add a Notification Configuration to Your Bucket](https://docs.aws.amazon.com/AmazonS3/latest/dev/ways-to-add-notification-config-to-bucket.html#step2-enable-notification)

Configure it to publish "s3:ObjectCreated:*" events,
and no other events.
