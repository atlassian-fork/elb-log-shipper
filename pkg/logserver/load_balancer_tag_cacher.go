package logserver

import (
	"sync"
	"time"
)

type CacheEntry struct {
	timePut time.Time
	tags    map[string]string
}

type LoadBalancerTagCacher struct {
	CleanupInterval time.Duration
	ExpirationTime  time.Duration

	cacheMutex       sync.RWMutex
	cache            map[string]CacheEntry
	cacheLastCleaned time.Time
}

func NewLoadBalancerTagCacher() *LoadBalancerTagCacher {
	return &LoadBalancerTagCacher{
		CleanupInterval: time.Minute,
		ExpirationTime:  time.Minute,

		cache: make(map[string]CacheEntry),
	}
}

func (c *LoadBalancerTagCacher) GetTags(key string) (map[string]string, bool) {
	c.clean()

	c.cacheMutex.RLock()
	defer c.cacheMutex.RUnlock()

	entry, ok := c.cache[key]
	return entry.tags, ok
}

func (c *LoadBalancerTagCacher) PutTags(key string, tags map[string]string) {
	c.clean()

	c.cacheMutex.Lock()
	defer c.cacheMutex.Unlock()

	c.cache[key] = CacheEntry{
		timePut: time.Now(),
		tags:    tags,
	}
}

func (c *LoadBalancerTagCacher) clean() {
	c.cacheMutex.Lock()
	defer c.cacheMutex.Unlock()

	if time.Since(c.cacheLastCleaned) < c.CleanupInterval {
		return
	}

	for key, entry := range c.cache {
		if time.Since(entry.timePut) > c.ExpirationTime {
			delete(c.cache, key)
		}
	}
	c.cacheLastCleaned = time.Now()
}
