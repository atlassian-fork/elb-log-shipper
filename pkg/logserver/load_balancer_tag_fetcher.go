package logserver

import (
	"context"
	"fmt"

	log "github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"
)

type LoadBalancerTagFetcher struct {
	ElbService   ElbTagDescriber
	Elbv2Service Elbv2TagDescriber

	// Since ELBs and ALBs have different name formats, it's fine to share a cache between them.
	Cacher TagCacher
}

func (f *LoadBalancerTagFetcher) FetchTagsElb(ctx context.Context, name string) (map[string]string, error) {
	if tags, ok := f.Cacher.GetTags(name); ok {
		return tags, nil
	}
	output, err := f.ElbService.DescribeTagsElb(ctx, &elb.DescribeTagsInput{
		LoadBalancerNames: []*string{aws.String(name)},
	})
	if err != nil {
		// TODO: ensure it's a LoadBalancerNotFound error.
		f.Cacher.PutTags(name, nil)
		return nil, ContextOrError(ctx, "ELB DescribeTags error: %v", err)
	}
	tags := make(map[string]string)
	for _, description := range output.TagDescriptions {
		if *description.LoadBalancerName != name {
			log.WithFields(log.Fields{
				"load_balancer_name": *description.LoadBalancerName,
			}).Warningf("unexpected TagDescription for ELB")
			continue
		}
		for _, tag := range description.Tags {
			tags[*tag.Key] = *tag.Value
		}
	}
	f.Cacher.PutTags(name, tags)
	return tags, nil
}

func (f *LoadBalancerTagFetcher) FetchTagsElbv2(ctx context.Context, region string, accountId string, name string) (map[string]string, error) {
	if tags, ok := f.Cacher.GetTags(name); ok {
		return tags, nil
	}
	albArn := fmt.Sprintf("arn:aws:elasticloadbalancing:%s:%s:loadbalancer/%s", region, accountId, name)
	output, err := f.Elbv2Service.DescribeTagsElbv2(ctx, &elbv2.DescribeTagsInput{
		ResourceArns: []*string{aws.String(albArn)},
	})
	if err != nil {
		// TODO: ensure it's a LoadBalancerNotFound error.
		f.Cacher.PutTags(name, nil)
		return nil, ContextOrError(ctx, "ELBv2 DescribeTags error: %v", err)
	}
	tags := make(map[string]string)
	for _, description := range output.TagDescriptions {
		if *description.ResourceArn != albArn {
			log.WithFields(log.Fields{
				"load_balancer_arn": *description.ResourceArn,
			}).Warningf("unexpected TagDescription for ALB with ARN")
			continue
		}
		for _, tag := range description.Tags {
			tags[*tag.Key] = *tag.Value
		}
	}
	f.Cacher.PutTags(name, tags)
	return tags, nil
}
