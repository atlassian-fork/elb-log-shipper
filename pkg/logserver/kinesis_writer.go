package logserver

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/rand"

	"github.com/DataDog/datadog-go/statsd"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/golang/protobuf/proto"
)

const (
	partitionKeyGenLength  = 16
	kplMagicNumber         = "\xF3\x89\x9A\xC2"
	volumeMetric           = "volume_pushed_logs"
	processedRecordsMetric = "processed_records"
	putProtosMetric        = "put_protos"
)

type KinesisWriter struct {
	KinesisService     Kinesis
	Statsd             *statsd.Client
	LogStreamName      string
	Environment        string
	ServiceId          string
	SourceType         string
	LogInfo            *LogFileInfo
	MaxRecordDataSize  uint64
	MaxShardRetries    int
	LogEntryCustomiser func(logEntry LogEntry) interface{}

	bufferedRecord AggregatedRecord
	bufferDataSize uint64
}

func (w *KinesisWriter) PushEntry(ctx context.Context, logLineEntry *LogLineEntry) error {
	logEntry := LogEntry{
		LogFileInfo:  *w.LogInfo,
		LogLineEntry: *logLineEntry,
		Environment:  w.Environment,
		ServiceId:    w.ServiceId,
		SourceType:   w.SourceType,
	}

	record := &Record{
		PartitionKeyIndex: proto.Uint64(0),
	}

	var err error
	if w.LogEntryCustomiser != nil {
		record.Data, err = json.Marshal(w.LogEntryCustomiser(logEntry))
	} else {
		record.Data, err = json.Marshal(logEntry)
	}
	if err != nil {
		return fmt.Errorf("error marshalling %+v: %v", logEntry, err)
	}
	return w.pushRecord(ctx, record)
}

func (w *KinesisWriter) pushRecord(ctx context.Context, record *Record) error {
	// Since each record contains a log entry corresponding to a single log line, this should always be much less than the
	// MaxRecordDataSize.
	recordSize := uint64(len(record.Data))
	if recordSize > w.MaxRecordDataSize {
		return fmt.Errorf("record data size (%d) exceeds MaxRecordDataSize (%d)", recordSize, w.MaxRecordDataSize)
	}
	if w.bufferDataSize+recordSize > w.MaxRecordDataSize {
		if err := w.Flush(ctx); err != nil {
			return err
		}
	}
	w.bufferedRecord.Records = append(w.bufferedRecord.Records, record)
	w.bufferDataSize += recordSize
	return nil
}

func (w *KinesisWriter) Flush(ctx context.Context) error {
	if w.bufferDataSize == 0 {
		return nil
	}

	var err error
	for i := 0; i < w.MaxShardRetries; i++ {
		var partitionKey string
		if partitionKey, err = randPartitionKey(); err != nil {
			return err
		}
		w.bufferedRecord.PartitionKeyTable = []string{partitionKey}

		var marshalledRecord []byte
		marshalledRecord, err = proto.Marshal(&w.bufferedRecord)
		if err != nil {
			return fmt.Errorf("error marshalling into proto %+v: %s", w.bufferedRecord, err)
		}

		var data []byte
		data, err = kplAggregationPack(marshalledRecord)
		if err != nil {
			return fmt.Errorf("error during KPL aggregation: %v", err)
		}

		_, err = w.KinesisService.PutRecord(ctx, &kinesis.PutRecordInput{
			Data:         data,
			PartitionKey: aws.String(partitionKey),
			StreamName:   aws.String(w.LogStreamName),
		})
		if err == nil {
			tags := []string{
				fmt.Sprintf("load_balancer_name:%s", w.LogInfo.LoadBalancer.Name),
			}
			w.Statsd.Incr(putProtosMetric, tags, 1)
			w.Statsd.Count(processedRecordsMetric, int64(len(w.bufferedRecord.Records)), tags, 1)
			w.Statsd.Count(volumeMetric, int64(len(data)), tags, 1)
			w.bufferDataSize = 0
			w.bufferedRecord.Reset()
			return nil
		}
	}
	return ContextOrError(ctx, "kinesis error when flushing entries (tried %d times): %v", w.MaxShardRetries, err)
}

func kplAggregationPack(byteData []byte) ([]byte, error) {
	data := bytes.NewBufferString(kplMagicNumber)
	_, err := data.Write(byteData)
	if err != nil {
		return nil, fmt.Errorf("error writing marshalled record to buffer: %v", err)
	}

	md5sum := md5.Sum(byteData)
	_, err = data.Write(md5sum[:])
	if err != nil {
		return nil, fmt.Errorf("error writing marshalled record md5 sum to buffer: %v", err)
	}
	return data.Bytes(), nil
}

func randPartitionKey() (string, error) {
	randomChars := make([]byte, partitionKeyGenLength)
	if _, err := rand.Read(randomChars); err != nil {
		return "", fmt.Errorf("error reading random bytes: %v", err)
	}
	key := make([]byte, hex.EncodedLen(partitionKeyGenLength))
	_ = hex.Encode(key, randomChars)
	return string(key), nil
}
